<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('bookings', 'BookingsController');

Route::get('contact', function () {
        return view('contact');
});

//Route::get('/home', 'HomeController@index')->name('home');
//
//Route::get('/bookings', 'BookingsController@bookings');
//
//Route::post('/bookings/create', 'BookingsController@store');
//
//Route::get('/contact', 'BookingsController@contact');
